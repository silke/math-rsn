\documentclass[11pt,a4paper,english]{article}
\usepackage{
    geometry,  % Page layout
    amssymb,   % Maths symbols
    commath,   % Maths commands
    esint,     % More integrals
    siunitx,   % SI Units
    babel,     % Language support
    caption,   % Improved captions
    xparse,    % LaTeX3 commands
    parskip,   % Use white space paragraph breaks
    booktabs,  % Table style improvements
    tabularx,  % Tab­u­lars with ad­justable-width columns
    multicol,  % Columns in text
}
\usepackage[scale=2]{ccicons}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[style,nomath,freefont]{utwentetitle}

\sisetup{inter-unit-product=\ensuremath{{}\cdot{}}}
\numberwithin{equation}{section}
\newcolumntype{L}{>{$}l<{$}}
\newcolumntype{C}{>{$}c<{$}}
\newcolumntype{P}{>{$}p{.3\textwidth}<{$}}

% Macros
\ExplSyntaxOn
\cs_set_eq:NN \macro \tl_gset:Nn
\ExplSyntaxOff

\macro\elsewh{\mathrm{elsewhere}}
\macro\mcr{\\[1em]}
\macro\intinf{\int_{-\infty}^\infty}
\macro\reals{\mathbb{R}}
\macro\isfrown{\stackrel{\frown}{=}}
\macro\limTinf{\lim_{T\rightarrow\infty}}
\macro\intT{\int_{-T}^{T}}

\macro\qinp{\cos(2\pi f_c t)}
\macro\qqup{\sin(2\pi f_c t)}

% Commands
\DeclareDocumentCommand\adcref{m}{\emph{Analog \& Digital Communications} (p. #1)}
\DeclareDocumentCommand\piecetwo{m m}{%
    \begin{cases}
        #1 \\
        #2
    \end{cases}
}
\DeclareDocumentCommand\exp{m}{e^{#1}}
\DeclareDocumentCommand\fourier{O{\tau} m}
    {\intinf #2 \exp{-j 2 \pi f #1} \dif #1}
\DeclareDocumentCommand\ifourier{O{\tau} m}
    {\intinf #2 \exp{j 2 \pi f #1} \dif f}

% Math operators
\DeclareMathOperator\var{var}

% Table spacing
\renewcommand{\arraystretch}{1.8}

% Title
\title{Mathematics}
\subtitle{for Random Signals and Noise}
\author{Silke Hofstra, Koen Zandberg}

\begin{document}
\maketitle
This document is meant to be a somewhat comprehensive summary of the most important laws, equations and relations necessary for \emph{Random Signals and Noise}.

\tableofcontents

\vfill

\begin{minipage}{.65\textwidth}
    This document is published under the
    \emph{Creative Commons Attribution-ShareAlike 4.0 International} licence.
    \texttt{https://creativecommons.org/licenses/by-sa/4.0/}
\end{minipage}
\hfill
\begin{minipage}{.25\textwidth}
    \raggedleft \ccbysa
\end{minipage}

\clearpage

\section{Symbols and abbreviations}

\subsection{Abbreviations}

\begin{tabularx}{\textwidth}{l L l}
    \toprule
    Abbreviation & \text{Symbol} & Description \\
    \midrule
    ACF & R_X       & Autocorrelation function \\
    CCF & R_{XY}    & Cross-correlation function \\
    CDF & F_X       & Cumulative distribution function \\
    ESD & \Psi_X    & Energy spectral density \\
    LTI &           & Linear time invariant \\
    PDF & f_X       & Probability distribution function \\
    PSD & S_X       & Power spectral density \\
    RV  &           & Random variable \\
    WS  &           & Wide sense \\
    WSS &           & Wide sense stationary \\
    \bottomrule
\end{tabularx}

\subsection{Symbols}

\begin{tabularx}{\textwidth}{CcX}
    \toprule
    \text{Symbol} & Value/Unit & Description \\
    \midrule
    R_x(\tau) & & Autocorrelation function \\
    \bottomrule
\end{tabularx}

\clearpage

\section{Probability Theory}

\subsection{Distributions}

\begin{table}[ht]
\everymath{\displaystyle}
\begin{tabularx}{\textwidth}{X L L L}
    \toprule
    Distribution & P(X=k), \; k \in \mathbb{N} & E(X) & \var(X) \\
    \midrule
    Geometric &
        (1-p)^{k-1} p \hfill k \geq 1 &
        \frac{1}{p} &
        \frac{1-p}{p^2} \mcr
    Poisson ($\mu$) &
        \frac{\mu^k}{k!} \exp{-\mu} &
        \mu &
        \mu \mcr
    $B(n,p)$ &
        \binom{n}{k} p^k (1-p)^{n-k} &
        np &
        np(1-p) \mcr
    Hypergeometric ($N$, $R$, $n$) &
        \frac{\binom{R}{k} \binom{N-R}{n-k}}{\binom{N}{n}} &
        n\frac{R}{N} &
        n\frac{R}{N} \left(1-\frac{R}{N}\right) \frac{N-n}{N-1} \\
    \midrule
    \midrule
    Distribution & f_X(x) & E(X) & \mathrm{var}(X) \\ \hline
    Uniform ($a$, $b$) &
        \piecetwo{ \frac{1}{b-a} & x \in [a,b] }{ 0 & \elsewh } &
        \frac{a+b}{2} &
        \frac{1}{12}(b-a)^2 \mcr
    Exponential ($\lambda$) &
        \piecetwo{ \lambda\exp{-\frac12 x^2} & x \geq 0 }{ 0 & \elsewh } &
        \frac{1}{\lambda} &
        \frac{1}{\lambda^2} \mcr
    Standard Normal &
        \frac{1}{\sqrt{2\pi}} \exp{-\frac12 x^2} \hfill x \in \mathbb{R} &
        0 &
        1 \\
    Normal / Gaussian &
        \frac{1}{\sigma \sqrt{2\pi}} \exp{-\frac12 \left(\frac{x-\mu}{\sigma}\right)^2}
        \quad x \in \mathbb{R} &
        \mu &
        \sigma^2 \\
    \bottomrule
\end{tabularx}
\end{table}

\subsection{Expectation of a random variable}

\begin{table}[ht]
\everymath{\displaystyle}
\begin{tabularx}{\textwidth}{X L}
Correlation & R_{XY} = E[ X \cdot Y ] = \intinf \intinf x y f_{X,Y}(x,y) \dif x \dif y \\
Covariance  & C_{XY} = E[ (X - \mu_x)(Y - \mu_y)] = R_{XY} - \mu_X \mu_Y \\
\end{tabularx}
\end{table}

Orthogonal if $ R_{XY} = 0 $.
Uncorrelated if $ C_{XY} = 0 $.

Independent RVs have $ R_{XY} = \mu_X \mu_Y $, hence $ C_{XY} = 0 $.

\subsection{Conditional probability}

\begin{table}[ht]
\everymath{\displaystyle}
\begin{tabularx}{\textwidth}{X L}
Conditional PDF  & f_{X|Y}(x,y) = \frac{ f_{X,Y}(x,y) }{ f_Y(y) } \\
Conditional mean & E[X|Y=y] = \intinf x f_{X|Y}(x,y) \dif x \\
Bayes rule       & f_{A|B}(a,b) = \frac{ f_{B|A}(b,a) f_A(a) }{ \intinf f_{B|A}(b,u) f_A(u) \dif u } \\
\end{tabularx}
\end{table}


\subsection{Moments of random variables}

\begin{table}[ht]
\everymath{\displaystyle}
\begin{tabularx}{\textwidth}{X L}
Expectation/mean              & E[X] = \mu_x \intinf x f_X(x) \dif x \\
Expectation of function of RV & E[g(X)] = \intinf g(x) \cdot f_X(x) \dif x \\
Second moment                 & E[X^2] = \intinf x^2 f_X(x) \dif x \\
Linearity                     & E[a \cdot X + b \cdot Y] = a \cdot E[X] + b \cdot E[Y] \\
Variance                      & \sigma_X^2 = E[(X-\mu_x)^2] = E[X^2] - \mu_x^2 \\
\end{tabularx}
\end{table}

\subsection{Central limit theorem}
If $Z = \sum_{i=1}^N X_i $, where $X_i$'s are independent RVs, all having the same arbitrary distribution $f_X(x)$ with mean $\mu_X$ an variance $\sigma_X^2$, then:
\begin{align*}
    \mu_Z      &= E[Z] = \sum_{i=1}^N E[X_i] = N \mu_X \\
    \sigma_Z^2 &= \sum_{i=1}^N \sigma^2_{X_i} = N \sigma_X^2 \\
    f_Z(z)     &= \left( f_X * f_X * \dots * f_X \right)(z)
\end{align*}

For sufficiently large N, this is approximately Gaussian.

\clearpage
\section{Signals}
\subsection{Power and energy}
Instantaneous signal power:

\[ P_x(t) \stackrel{\frown}{=} |x(t)|^2 \]

Mean power:

\begin{align*}
    P_x &= A[P_x(t)] \\
        &= \lim_{T \rightarrow \infty} \frac{1}{2T} \intT P_x(t) \dif t \\
        &= \intinf S_x(f) \dif f \\
\end{align*}

For WSS processes:

\[ P_X = E[P_X(t)] = E\left[ |X(t)|^2 \right] = \intinf S_X(f) \dif f \]

Signal energy:

\begin{align*}
    E_x &\stackrel{\frown}{=} \int_{-\infty}^\infty P_x(t) \dif t \\
        &= \int_{-\infty}^\infty \Psi_x(f) \dif f \\
        &\geq 0
\end{align*}

\subsection{Autocorrelation function}
\subsubsection{Energy signals}
Definition:
\[ R_x(\tau) = \intinf x(t) x^*(t-\tau) \dif t \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ R_x(0) = E_x $
    \item $ R_x(\tau) = R^*_x(-\tau) $
    \item $ |R_x(\tau)| \leq R_x(0) $
    \item $ \intinf R_x(\tau) \dif \tau = \Psi_x(0) $
    \item For an LTI system:
          $ R_y(\tau) = R_x(\tau) * h(\tau) * h^*(-\tau) $
\end{enumerate}

\subsubsection{Power signals}
Definition:
\[ R_x(\tau) = \limTinf \frac{1}{2T} \intT x(t) x^*(t-\tau) \dif t \]

\begin{enumerate}
    \everymath{\displaystyle}
    \item $ R_x(0) = P_x $
    \item $ R_x(\tau) = R^*_x(-\tau) $
    \item $ |R_x(\tau)| \leq R_x(0) $
    \item $ \intinf R_x(\tau) \dif \tau = \S_x(0) $
    \item For an LTI system:
          $ R_y(\tau) = R_x(\tau) * h(\tau) * h^*(-\tau) $
    \item If $x(t)$ contains a constant term $\mu_x$,
          ACF contains a constant term $|\mu_x|^2$
    \item If $x(t)$ is periodic with period $T_p$,
          ACF is also periodic with period $T_p$ and:
          \begin{flalign*}
          \mu_x     &= \frac{1}{T_p} \int_{t_0}^{t_0+T_p} x(t) \dif t &\\
          R_x(\tau) &= \frac{1}{T_p} \int_{t_0}^{t_0+T_p} x(t) x^*(t-\tau) \dif t \\
          \end{flalign*}
\end{enumerate}

\subsection{Cross-correlation function}
Definition:
\[ R_{xy}(\tau) = \intinf x(t)y^*(t-\tau) \dif t \]

Signals are orthogonal over the entire time interval if:
\[ R_{xy}(0) = 0 \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ R_{xy}(\tau) = R^*_{yx}(-\tau) $
    \item $ |R_xy(\tau)| \leq \sqrt{R_x(0) R_y(0)} $
    \item $ |R_xy(\tau)| \leq \frac12 \left( R_x(0) + R_y(0) \right)$
    \item For an LTI system:
        \begin{flalign*}
        R_{xy}(\tau) &= R_x(\tau) * h^*(-\tau) & \\
        R_y(\tau)    &= R_{xy}(\tau) * h(\tau)
        \end{flalign*}
\end{enumerate}

Properties of the CCF for jointly WSS processes:

\begin{enumerate}
    \everymath{\displaystyle}
    \item $ R_{XY} = R_{XY}^*(-\tau) $
    \item $ |R_{XY}(\tau)| \leq \sqrt{R_X{0} R_Y(0)} $
    \item $ |R_{XY}(\tau)| \leq \frac12 [R_X{0} + R_Y(0)]$
\end{enumerate}

\subsection{Wiener-Khinchin relations}
Energy signals:
\begin{align*}
    \Psi_x(f) &= \fourier{R_x(\tau)}  \\
    R_x(\tau) &= \ifourier{\Psi_x(f)} \\
\end{align*}

Power signals:
\begin{align*}
    S_x(f)    &= \fourier{R_x(\tau)} \\
    R_x(\tau) &= \ifourier{S_x(f)}   \\
\end{align*}

\subsection{Energy spectral density}
Definition:
\[ \Psi_X(f) = |X(f)|^2 \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ \Psi_x(f) \geq 0 $
    \item $ \Psi_x(f) = \Psi_x(-f)$ for $x(t) \in \reals$
    \item $ \intinf \Psi_x(f) \dif f = E_x $
    \item For an LTI system:
          $ \Psi_y(f) = |H(f)|^2 \Psi_x(f) $
\end{enumerate}

\subsection{Cross energy spectral density}
Definition:
\[ \Psi_{xy}(f) = X(f) Y^*(f) \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ \Psi_{xy}(f) = \Psi^*_{yx}(f) $
    \item $ \Psi_{xy}(f) = \Psi^*_{xy}(-f) $
    \item $ \intinf \Psi_{xy}(f) \dif f = R_{xy}(0) $
    \item For an LTI system:
        \begin{flalign*}
        \Psi_{xy}(f) &= H^*(f) \Psi_x(f) &\\
                     &= X(f) Y^*(f) \\
                     &= X(f) H^*(f) X^*(f) \\
                     &= H^*(f) |X(f)|^2 \\
        \end{flalign*}
\end{enumerate}

\subsection{Power spectral density}
Definition:
\[ S_x(f) \stackrel{\frown}{=} \limTinf \frac{|X_T(f)|^2}{2T} \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ S_x(f) \leq 0 $
    \item $ S_x(f) = S_x(-f) $
    \item $ \intinf S_x(f) \dif f = P_x $
    \item For an LTI system: $ S_y(f) = |H(f)|^2 S_x(f) $
    \item If $x(t)$ contains a constant term $\mu_x$,
          PSD contains a term $|\mu_x|^2 \delta(f)$
    \item If $x(t)$ is periodic with period $T_p$,
          PSD contains delta functions at frequencies that are multiples of $\frac{1}{T_p}$
\end{enumerate}

Properties for WSS processes:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ S_X(f) \geq 0 $
    \item $ S_X(f) = S_X(-f) $
    \item $ \intinf S_X(f) \dif f = E[P_X(t)] $
    \item If $ X(t) $ contains a (random) DC term,
          $ X_{DC} = \limTinf \frac{1}{2T} \intT X(t) \dif t $, \\
          the PSD contains a term $ E\left[|X_{DC}|^2\right] \delta(f) $
    \item If $ X(t) $ is periodic with period $ T_p $,
          the PSD consists of delta functions at frequencies that are multiples of $ \frac{1}{T_p} $
\end{enumerate}


\subsection{Cross power spectral density}
Definition:
\[ S_x(f) \stackrel{\frown}{=} \limTinf \frac{X_T(f) Y_T^*(f)}{2T} \]

Properties:
\begin{enumerate}
    \everymath{\displaystyle}
    \item $ S_{xy}(f) = S^*_{yx}(f) $
    \item $ S_{xy}(f) = S^*_{xy}(-f) $
    \item $ \intinf S_{xy}(f) \dif f = R_{xy}(0) $
    \item For an LTI system: $ S_{xy}(f) = H^*(f) S_x(f) $
\end{enumerate}

\clearpage
\section{Random signals}

\subsection{Stationary}

A first order stationary process does not depend on time, and is \emph{stationary in the mean}.

\begin{tabular}{l L}
    First order stationary  & f_X(x_1; t_1) = f_X(x_1; t_1 + \tau) \\
    Second order stationary & f_X(x_1,x_2; t_1,t_2) = f_X(x_1,x_2;t_1+\tau,t_2+\tau) = f_X(x_1,x_2;t_1-t_2) \\
    Nth order stationary    & f_X(x_1,\dots,x_N; t_1,\dots,t_N) = f_X(x_1,\dots,x_2; t_1+\tau,\dots,t_N+\tau) \\
\end{tabular}

A process is wide-sense stationary (WSS) if:

\begin{enumerate}
    \everymath{\displaystyle}
    \item $ \mu_X(t) = E[X(t)] = \mu_X $ for all $t$.
    \item $ R_X(t, t-\tau) = E[X(t) X^*(t-\tau)] = R_X(\tau) $ for all $t$.
\end{enumerate}

A second order stationary process is also wide-sense stationary.

\subsection{Ergodicity}

A process is \emph{ergodic in the mean} if:

\[ \mu_X = A[x_n(t)] = \lim_{T \rightarrow \infty} \frac{1}{2T} \int_{-T}^T x_n(t) \dif t \]

A process is wide-sense ergodic if:

\[
    R_{x_n}(\tau)
    = A[x_n(t) x^*(t - \tau)]
    = \lim_{T \rightarrow \infty} \frac{1}{2T} \int_{-T}^T x_n(t) x^*(t - \tau) \dif t
\]

\subsection{Overview}

\begin{table}[ht]
\renewcommand{\arraystretch}{2.4}
\begin{tabular}{l L}
    Wide-sense stationary       & \piecetwo{ E[X(t)] = \mu_x }
                                    { R_X(t, t - \tau) = R_X(\tau) } \\
    Wide-sense ergodic          & \piecetwo{ A[X(t)] = E[X(t)] }
                                    { A[X(t) X^*(t - \tau)] = R_X(\tau) } \\
    Joint wide-sense stationary & \piecetwo{ X(t), Y(t) \rightarrow \text{wide-sense stationary} }
                                    { R_{XY}(t, t - \tau) = R_{XY}(\tau) } \\
    Joint wide-sense ergodic    & \piecetwo{ X(t), Y(t) \rightarrow \text{wide-sense ergodic} }
                                    { A[X(t) Y^*(t-\tau)] = R_{XY}(\tau) }\\
\end{tabular}
\end{table}

\clearpage

\subsection{Jointly Gaussian processes}
Two processes are \emph{jointly Gaussian processes} if any combination of samples of \\
$ X(t_1), \dots, X(t_N), Y(t_1'), \dots, Y(t_M') $
can be described by the multivariate Gaussian PDF:

\[
    f_Z(\vec{z}; \vec{t}) =
    \sqrt{ \frac{|\vec{C_Z}^{-1}|}
                {(2\pi)^N}
    }
    \exp{
        -\frac12 \left[ (\vec{z} - \vec{\mu_Z})^T \vec{C_Z}^{-1}(\vec{z} - \vec{\mu_Z}) \right]
    }
\]

With:

\begin{align*}
    \vec{C_Z}   &= \begin{bmatrix}
                       \vec{C_X}    & \vec{C_{YX}} \\
                       \vec{C_{XY}} & \vec{C_{Y}}  \\
                   \end{bmatrix} \\
    \vec{Z}     &= [ X(t_1), \dots, X(t_N), Y(t_1'), \dots, Y(t_M') ]^T \\
    \vec{z}     &= [ x_1, \dots, x_N, y_1, \dots, y_M ]^T \\
    \vec{t}     &= [ t_1, \dots, t_N, t_1', \dots, t_M' ]^T \\
    \vec{\mu_Z} &= E[\vec{Z}] = [ \mu_X(t_1), \dots, \mu_X(t_N), \mu_Y(t_1'), \dots, \mu_Y(t_N') ]\\
    C_{\vec{X}, ij}  &= C_{X(t_i) X_(t_j)}  = C_X (t_i, t_j) \\
    C_{\vec{XY}, ij} &= C_{X(t_i) Y_(t_j')} = C_{XY} (t_i, t_j') \\
\end{align*}

Properties:

\begin{enumerate}
    \everymath{\displaystyle}
    \item The joint statistics of jointly Gaussian processes $ X(t) $ and $ Y(t) $ are fully specified by their expected values $ \mu_X(t) $ and $ \mu_Y(t) $,
          their ACFs $ R_X(t_1, t_2) $ and $ R_Y(t_1, t_2) $, and their CCF $ R_{XY}(t_1, t_2) $
    \item If two jointly Gaussian random processes are uncorrelated,
          then they are also independent.
    \item When the input signal of an LTI system is a Gaussian process,
          then the input signal and output signal are jointly Gaussian processes
\end{enumerate}

\subsection{Overview: LTI filtering of WSS random processes}

\begin{tabularx}{\textwidth}{LLL}
    \toprule
    \text{Input} & \text{Intermediate} & \text{Output} \\
    \midrule
    % Signals
    X(t) & h(t) & Y(t) = X(t) * h(t) \\
    X(f) & H(f) & Y(f) = X(f) H(f) \\

    % Mean
    \mu_X = E[X(t)] & & \mu_Y = \mu_X \intinf h(\rho) \dif \rho = \mu_X H(0) \\

    % Variation
    \sigma_X^2 = E[X^2(t)] & & E[Y^2(t)] \\

    % Autocorrelation / Cross correlation
    R_X(\tau) & & R_Y(\tau) = R_X(\tau) * h(\tau) * h^*(-\tau) \\

    & R_{XY}(\tau) = R_X(\tau) * h^*(-\tau) & R_Y(\tau) = R_{XY}(\tau) * h(\tau)    \\
    & R_{YX}(\tau) = R_X(\tau) * h(\tau)    & R_Y(\tau) = R_{YX}(\tau) * h^*(-\tau) \\

    % PSD
    S_X(f) & & S_Y(f) = S_X(f) |H(f)|^2 \\
    & S_{XY}(f) = S_X(f) H^*(f) & S_Y(f) = S_{XY}(f) H(f) \\
    & S_{YX}(f) = S_X(f) H(f)   & S_Y(f) = S_{YX}(f) H^*(f) \\


    \bottomrule
\end{tabularx}

\section{Noise}

\subsection{Gaussian noise}

Thermal noise (WSS Gaussian):

\[ S_V(f) = \frac{ 2 R h|f| }{ \exp{\frac{h|f|}{kT}} - 1 } \]

White noise:

\begin{multicols}{2}
\begin{align*}
    \mu_X          &= 0 \\
    S_X(f)         &= \frac{N_0}{2} \\
    S_{XY}(\omega) &= \frac{N_0}{2} H(\omega) \\
\end{align*}

\columnbreak
\begin{align*}
    \sigma_X       &= \frac{N_0}{2} \\
    R_X(\tau)      &= \frac{N_0}{2} \delta(\tau) \\
    R_{XY}(\tau)   &= \frac{N_0}{2} h(\tau) \\
\end{align*}
\end{multicols}

\subsection{Equivalent noise bandwidth}

\section{Signal space}

\subsection{Definition}

\begin{tabularx}{\textwidth}{X L}
    Inner product &
        \langle x(t), y(t) \rangle = \intinf x(t) y(t) \dif t = R_{xy}(0) \\
    Orthonormal set &
        \{ \phi_k(t) \}:
        \langle \phi_k(t), \phi_l(t) \rangle
        = \delta_{kl}
        = \piecetwo{1, \quad k = l}{0, \quad k \neq l} \\
    Complete set &
        s(t) = \sum_{k=1}^K s_k \phi_k(t)
        \Leftrightarrow
        s_k = \langle s(t), \phi_k(t) \rangle = \intinf s(t) \phi_k(t) \dif t \\
\end{tabularx}

The set $ \{ s_i \} $ can be considered as elements of a vector $ \vec{s} $.
The corresponding vector space is called the signal space $ \{ \phi_{k}(t) \} $ is the orthonormal basis of the signal space.

\subsection{Gram-Schmidt orthogonalisation}

\begin{enumerate}
    \everymath{\displaystyle}
    \item $ \phi_1(t)
            = \frac{s(t)}{\sqrt{\langle s_1(t), s_1(t)}}
            = \frac{s_1(t)}{\sqrt{R_{s_1}(0)}} $
    \item $ g_2(t)
            = s_2(t) - \langle s_2(t), \phi_1(t) \rangle \cdot \phi_1(t)
            = s_2(t) - R_{s_2 \phi_1}(0) \cdot \phi_1(t) $
    \item $ \phi_2(t)
            = \frac{g_2(t)}{\sqrt{\langle g_2(t), g_2(t)}}
            = \frac{g_2(t)}{\sqrt{R_{g_2}(0)}} $
\end{enumerate}

Generalisation:

\begin{align*}
    g_k(t)    &= s_k(t) - \sum_{j=1}^{k-1} \langle s_k(t), \phi_j(t) \rangle \cdot \phi_j(t)
               = s_k(t) - \sum_{j=1}^{k-1} R_{s_k \phi_j}(0) \cdot \phi_j \\
    \phi_k(t) &= \frac{g_k(t)}{\sqrt{\langle g_k(t), g_k(t) \rangle}}
               =  \frac{g_k(t)}{\sqrt{R_{g_k}(0)}} \\
\end{align*}

\subsection{Noise}

\begin{align*}
    R(t)    &= s_i(t) + N(t) \\
    r_k     &= \langle R(t), \phi_k(t)   \rangle = \intinf R(t) \phi_k(t) \dif t = s_{i,k} + N_k \\
    s_{i,k} &= \langle s_i(t), \phi_k(t) \rangle = \intinf s_i(t) \phi_k(t) \dif t \\
    N_k     &= \langle N(t), \phi(t)     \rangle = \intinf N(t) \phi_k(t) \dif t \\
\end{align*}

\section{Filter design}

\section{Error rate}

\subsection{BER}

\[ P_e = Q\left( \frac{\mu}{2 \sigma} \right) = \left( \sqrt{\frac{E_d}{2 N_0}} \right) \]

\section{Narrowband}
$ s(t) = a(t) \cos(2 \pi f_c t + \phi(t)) = s_I(t) \qinp - s_Q(t) \qqup $

\section{WSS random bandpass process}
\begin{itemize}
    \everymath{\displaystyle}
    \item $ R_N(\tau) = R_{N_I}(\tau)\cos(2\pi f_c \tau) + R_{N_I N_Q}(\tau)\sin(2\pi f_c \tau) $
    \item $ S_N(f) = \frac12 S_{N_I}(f+f_c)
            + \frac12 S_{N_I} (f-f_c)
            + \frac12 j S_{N_I N_Q}(f+f_c)
            - \frac12 j S_{N_I N_Q}(f-f_c) $
\end{itemize}
\end{document}
